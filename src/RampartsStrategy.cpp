/*
 * RampartsStrategy.cpp
 *
 *  Created on: Feb 23, 2016
 *      Author: WiredCats
 */

#include "RampartsStrategy.h"

RampartsStrategy::RampartsStrategy()
	: AutonomousStrategy(RAMPARTS)
{
	m_breachSpeed = 0.8;
	m_breachDistance = 175.0;
}

RampartsStrategy::~RampartsStrategy()
{
}

void RampartsStrategy::ProcessApproach()
{
	if (m_activeMode == APPROACH)
	{
		ProcessGoStraight(m_approachTime, m_approachSpeed, BREACH);
	}
}

void RampartsStrategy::ProcessBreach()
{
	if (m_activeMode == BREACH)
	{
		ProcessGoStraight(m_breachTime, m_breachSpeed, DISABLE);
	}
}

