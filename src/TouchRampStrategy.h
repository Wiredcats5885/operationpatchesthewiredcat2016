#pragma once
#include "AutonomousStrategy.h"

class TouchRampStrategy : public AutonomousStrategy
{
public:
	TouchRampStrategy();
	virtual ~TouchRampStrategy();

// methods
protected:
	virtual void ProcessApproach();
	virtual void ProcessBreach();
};
