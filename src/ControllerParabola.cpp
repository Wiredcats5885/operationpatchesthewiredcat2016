#include "iostream"
#include "math.h"
#include "ControllerParabola.h"


ControllerParabola::ControllerParabola()
	: m_a(0.0)
	, m_b(0.0)
{
}

ControllerParabola::~ControllerParabola()
{
}

float ControllerParabola::FOfX(float x)
{
	float y = (m_a * x * x) + (m_b * x);
	return y;
}

void ControllerParabola::SetPoints(float x2, float y2, float x3, float y3)
{
	m_b = ((y3 * x2 * x2) - y2) / ((x2 * x2) - x2);
	m_a = y3 - m_b;
}
