#include "LowBarStrategy.h"

LowBarStrategy::LowBarStrategy()
	: AutonomousStrategy(LOW_BAR)
{
	m_breachSpeed = m_approachSpeed;
	m_breachDistance = 70.0;
}

LowBarStrategy::~LowBarStrategy()
{
}

void LowBarStrategy::ProcessApproach()
{
	if (m_activeMode == APPROACH)
	{
		ProcessGoStraight(m_approachTime, m_approachSpeed, BREACH);
	}
}

void LowBarStrategy::ProcessBreach()
{
	if (m_activeMode == BREACH)
	{
		ProcessGoStraight(m_breachTime, m_breachSpeed, DISABLE);
	}
}
