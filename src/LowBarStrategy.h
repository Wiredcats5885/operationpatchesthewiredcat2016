#pragma once 
#include "AutonomousStrategy.h"

class LowBarStrategy : public AutonomousStrategy
{
public:
	LowBarStrategy();
	virtual ~LowBarStrategy();
	
// methods
protected:
	virtual void ProcessApproach(); 
	virtual void ProcessBreach();
};
