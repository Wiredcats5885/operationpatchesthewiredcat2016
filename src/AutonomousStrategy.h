#pragma once
#include "definitions.h"
#include "WPILib.h"

class AutonomousStrategy
{
public:
	// data types
	enum AutonomousDefenses
	{
		DEFENSE_UNDEFINED,
		TOUCH_RAMP,
		LOW_BAR,
		ROCK_WALL,
		ROUGH_TERRAIN,
		RAMPARTS,
		MOAT,
		SALLY_PORT,
		PORTCULLIS,
		DRAW_BRIDGE
	};

	enum AutonomousMode
	{
		WAIT,
		APPROACH,
		BREACH,
		DISABLE
	};

	AutonomousStrategy(AutonomousDefenses defenseType);
	virtual ~AutonomousStrategy();

// properties
protected:
	AutonomousDefenses m_defenseType;
	AutonomousMode m_activeMode;
	Timer m_timer;
	float m_waitTime, m_approachTime, m_breachTime;
	float m_approachSpeed, m_breachSpeed;
	float m_approachDistance, m_breachDistance;

	//declaring robots motors
	std::unique_ptr<CANTalon> m_motorRearLeft;
	std::unique_ptr<CANTalon> m_motorFrontLeft;
	std::unique_ptr<CANTalon> m_motorFrontRight;
	std::unique_ptr<CANTalon> m_motorRearRight;
	std::unique_ptr<CANTalon> m_motorBall;

// methods
protected:
	float CalculateMotorOnTime(float distance, float scaleFactor);
	void ProcessGoStraight(float maxTime, float motorSpeed, AutonomousMode newMode);
	virtual void ProcessDisable();
	virtual void ProcessWaiting();
	virtual void ProcessApproach() = 0;
	virtual void ProcessBreach() = 0;

public:
	virtual void RobotInit();
	void AutonomousPeriodic();
};
