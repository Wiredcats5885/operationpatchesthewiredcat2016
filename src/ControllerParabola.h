#pragma once

class ControllerParabola
{
public:
	ControllerParabola();
	~ControllerParabola();

private:
	float m_a, m_b; // a b values for parabolic equations

public:
	float FOfX(float x);
	void SetPoints(float x2, float y2, float x3, float y3);

};
