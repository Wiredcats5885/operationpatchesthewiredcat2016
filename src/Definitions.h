/*
 * Definitions.h
 *
 *  Created on: Feb 6, 2016
 *      Author: WiredCats
 */

#pragma once

//these is the register on the x-box 360 controller

//these are used with GetRawAxis
#define JOYSTICK_RIGHT_X (4)
#define JOYSTICK_RIGHT_Y (5)
#define JOYSTICK_RIGHT_BUTTON (10)
//these are also used with GetRawAxis
#define JOYSTICK_LEFT_X (0)
#define JOYSTICK_LEFT_Y (1)
#define JOYSTICK_LEFT_BUTTON (9)

//these are used with GetRawButton
#define BUTTON_A (1)
#define BUTTON_B (2)
#define BUTTON_X (3)
#define BUTTON_Y (4)
#define BUTTON_START (8)
#define BUTTON_BACK (7)

//this is used with GetPOV
#define D_PAD (0)

//this is also used with GetRawAxis
#define TRIGGER_RIGHT (3)
#define TRIGGER_LEFT (2)

//this is also used with GetRawButton
#define BUMPER_RIGHT (6)
#define BUMPER_LEFT (5)

//we gotta figure this one out
#define RUMBLE_VIBRATE (0)
#define RUMBLE_RUMBLE (1)

// Output channels for RoboRio
#define MOTOR_REAR_LEFT (1)   // Initialize the Talon as device 1. Use the roboRIO web
#define MOTOR_FRONT_LEFT (2)
#define MOTOR_FRONT_RIGHT (3)
#define MOTOR_REAR_RIGHT (4)// interface to change the device number on the talon
#define MOTOR_WINCH (6)
#define MOTOR_BALL (5)
#define MOTOR_SCISSOR_1 (7)
#define MOTOR_SCISSOR_2 (8)
#define MOTOR_SCOOP (9)
#define CONTROLLER (0)
//this is the end of it

#define JOYSTICK_OFFSET (0.03)

enum RobotSide
{
	LEFT,
	RIGHT
};

extern bool Equals (double value1, double value2, double tolerance);
