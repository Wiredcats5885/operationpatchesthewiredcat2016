#include "RoughTerrainStrategy.h"

RoughTerrainStrategy::RoughTerrainStrategy()
	: AutonomousStrategy(ROUGH_TERRAIN)
{
	m_breachSpeed = 0.8;
	m_breachDistance = 130.0;
}

RoughTerrainStrategy::~RoughTerrainStrategy()
{
}

void RoughTerrainStrategy::ProcessApproach()
{
	if (m_activeMode == APPROACH)
	{
		ProcessGoStraight(m_approachTime, m_approachSpeed, BREACH);
	}
}

void RoughTerrainStrategy::ProcessBreach()
{
	if (m_activeMode == BREACH)
	{
		ProcessGoStraight(m_breachTime, m_breachSpeed, DISABLE);
	}
}
