//this imports all of our needed libraries for the program
//everything with a .h extension is a header file
//#include "iostream" (ignore this)
#include "math.h"
#include "WPILib.h"
#include <ControllerState.h>
#include <Definitions.h>
#include "TouchRampStrategy.h"
#include "LowBarStrategy.h"
#include "RockWallStrategy.h"
#include "RoughTerrainStrategy.h"
#include "RampartsStrategy.h"
#include "ControllerParabola.h"
#include "CANSpeedController.h"
#include <math.h>

//this is a method that will give you the absolute value whenever you need it
//(used multiple times in the program)
bool Equals (double value1, double value2, double tolerance)
{
	return (fabs(value1 - value2) < tolerance);
}

//this is the class constructor for the whole program
//everything in this class is ran when using the robot
//it loops and then runs this
class Robot : public IterativeRobot
{
private:
	//private means that classes outside of the robot cannot access anything in here

	//LiveWindow *lw = LiveWindow::GetInstance();
	//SendableChooser *chooser;
	//constants means you cannot change these variables
	//you can refer to standards but can never manipulate them
	const std::string autoNameDefault = "Default";//this cannot ever be changed
	const std::string autoNameCustom = "My Auto";//neither can this
	std::string autoSelected;//this can be changed because it is not a constant
	//std (standard) refers to the current class
	//the :: (double colon) refers to the scope ({} is a scope)

	//this is the state of any and all controls of the x-box controller
	//this includes but is not limited to buttons, joysticks, bumpers (everything)
	ControllerState m_controllerState; //X-Box Controller State
	//declaring robots motors
	std::unique_ptr<CANTalon> m_motorRearLeft;
	std::unique_ptr<CANTalon> m_motorFrontLeft;
	std::unique_ptr<CANTalon> m_motorFrontRight;
	std::unique_ptr<CANTalon> m_motorRearRight;
	std::unique_ptr<CANTalon> m_motorWinch;
	std::unique_ptr<CANTalon> m_motorBall;
	std::unique_ptr<CANTalon> m_motorScissor1;
	std::unique_ptr<CANTalon> m_motorScissor2;
	std::unique_ptr<CANTalon> m_motorScoop;

	//this is an array based off of our parabola for the values to accelerate cleanly
	//we used them by rounding the intake of our joysticks and rounding them to the
	//closest array value. This helps make it less touchy driving
	float m_motorSpeeds [17] = {1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,
			0.98078528,0.923879533,0.831469612,0.707106781,0.555570233,
			0.382683432,0.195090322,0.0};

	double maxTime = 6.0;


	// Auto-mode strategies
	//we are setting the classes of our different auto strategies to objects so we
	//can call them. You cannot access an class in a method unless you set it as an
	//object
	//TouchRampStrategy m_touchRampStrategy;
	//LowBarStrategy m_lowBarStrategy;
	//RockWallStrategy m_rockWallStrategy;
	//RoughTerrainStrategy m_roughTerrainStrategy;
	//RampartsStrategy m_rampartsStrategy;
	//AutonomousStrategy *m_pActiveAutoStrategy = nullptr;
	//the above astrics is for a pointer. When you run a computer everything runs off
	//the ram. So when you use the astrics you are getting the address of the active
	//auto strategy so that you can manipulate it

	//This is a timer for Auto mode.
	//For this we input the speed of the motors and the distance we want to travel
	//the program calculates how long it needs to run the motors to achieve that distance
	Timer m_autoTimer;
	double m_autoGoStraightTime = 0.0;//this has to have a default value so we set it
	// to 0.0 (doesn't always have to be 0)
	float m_autoGoStraightDistance = 122.0;
	float m_autoMotorSpeed = 0.6;

	// controller curve for tank mode joysticks
	//this is once again displaying a class being set as an object to that you can
	//manipulate it
	ControllerParabola m_joystickCurve;

	// Used for PWM control. Argument = channel number
	//PWM = pulse width modulation
	//this is declaring the victor motor for the robot arm
	std::unique_ptr<VictorSP> pRobotArm;// = new VictorSP(0);
	float m_downSensitivity = 0.3; // 0.3 (when you are going down, you will be going
	//-30 percent strength (the negative is set later))
	float m_upSensitivity = 0.7; // 0.7 (when you are going up you will be going 70
	//percent strength
	//*note going up is when you are going away from the robot and going down is going
	//towards the robot

	// DIO button input objects
	//These lines of code the different autocodes
	std::unique_ptr<DigitalInput> m_autoSelect4;
	std::unique_ptr<DigitalInput> m_autoSelect5;
	std::unique_ptr<DigitalInput> m_autoSelect6;

	//This is declaring the arm stopper with a DIO
	std::unique_ptr<DigitalInput> pArmStopper;// = new DigitalInput(9);

	//Robot arm encoder - NOT implemented
	//std::unique_ptr<Encoder> m_armEncoder;
	//m_armEncoder.reset(new Encoder(0,1,false,Encoder::EncodingType::k4X));
	//bool m_resetWasRun = false;
/*
	void ChooseAutonomousStrategy()
	{
		//these are the numbers that correspond with the below numbers
		//see note to see where the jumper cables must go
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		// Autonomous Modes
		// 0 = touch ramp
		// 1 = low bar
		// 2 = rough terrain
		// 3 = rockwall, moat
		// 4 = ramparts
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

		//this uses binary to read 5 volts (equals 1 in binary)
		//or false (equals 0 in binary)
		int bit0 = (m_autoSelect4->Get()) ? 1 : 0;
		int bit1 = (m_autoSelect5->Get()) ? 2 : 0;
		int bit2 = (m_autoSelect6->Get()) ? 4 : 0;
		int autonomousMode = bit0 + bit1 + bit2;

		//this is a switch case that takes the result of the jumper cables and then
		//goes to the corresponding case to run that code
		switch (autonomousMode)
		{
		case 1: // low bar DIO: 4 -> +5V, DIO 5 -> GND, DIO 6 -> GND
			m_pActiveAutoStrategy = &m_lowBarStrategy;
			break;

		case 2: //rough terrain: DIO 4 -> GND, DIO 5 -> +5v, DIO 6 -> GND
			m_pActiveAutoStrategy = &m_roughTerrainStrategy;
			break;

		case 3: // rockwall, moat: DIO 4 -> +5V, DIO 5 -> +5v, DIO 6 -> GND
			m_pActiveAutoStrategy = &m_rockWallStrategy;
			break;

		case 4: // ramparts: DIO 4 -> GND, DIO 5 -> GND, DIO 6 -> +5V
			m_pActiveAutoStrategy = &m_rampartsStrategy;
			break;

		//since you always need a default statement in switch statements, we set the
		//default to touch the ramp, just to be safe. So if the jumpercables do not
		//equal one of the case numbers, it will just touch the ramp
		default: // default to touch ramp: DIO 4 -> GND, DIO 5 -> GND, DIO 6 -> GND
			//m_pActiveAutoStrategy = &m_touchRampStrategy;
			m_pActiveAutoStrategy = &m_lowBarStrategy;
			break;
		}
	}
*/
	//This is called a void. It does not return anything. Instead, when the program
	//starts up, this is run to initialize everything in the program
	//Init stands for initialize. This is only run once every start up
	void RobotInit()
	{
		//this camera server stuff is for the camera. It sets the camera quality as well
		//as what camera to reference (we only have one camera though :P)
		CameraServer::GetInstance()->SetQuality(1); //starts camera capture instance and quality (0-100)
		CameraServer::GetInstance()->StartAutomaticCapture("cam1"); //camx refers to camera number based off of

		//These initialize all of the motors it sets each object as the proper motors
		//each motor is declared in the definition header file! :D
		m_motorRearLeft.reset(new CANTalon(MOTOR_REAR_LEFT));
		m_motorFrontLeft.reset(new CANTalon(MOTOR_FRONT_LEFT));
		m_motorFrontRight.reset(new CANTalon(MOTOR_FRONT_RIGHT));
		m_motorRearRight.reset(new CANTalon(MOTOR_REAR_RIGHT));
		m_motorWinch.reset(new CANTalon(MOTOR_WINCH));
		m_motorBall.reset(new CANTalon(MOTOR_BALL));
		m_motorScissor1.reset(new CANTalon(MOTOR_SCISSOR_1));
		m_motorScissor2.reset(new CANTalon(MOTOR_SCISSOR_2));
		m_motorScoop.reset(new CANTalon(MOTOR_SCOOP));

		//this declares the motors under a enum
		//enum (enumeration) means the declared objects can only have certain values
		m_motorRearLeft->SetControlMode(CANSpeedController::kPercentVbus);
		m_motorFrontLeft->SetControlMode(CANSpeedController::kPercentVbus);
		m_motorFrontRight->SetControlMode(CANSpeedController::kPercentVbus);
		m_motorRearRight->SetControlMode(CANSpeedController::kPercentVbus);
		m_motorWinch->SetControlMode(CANSpeedController::kPercentVbus);
		m_motorBall->SetControlMode(CANSpeedController::kPercentVbus);
		m_motorScissor1->SetControlMode(CANSpeedController::kPercentVbus);
		m_motorScissor2->SetControlMode(CANSpeedController::kPercentVbus);
		m_motorScoop->SetControlMode(CANSpeedController::kPercentVbus);

		//here with neutralmode_break means that when  we are not giving the
		//robot power to move, it breaks instead of gliding
		m_motorRearLeft->ConfigNeutralMode(CANSpeedController::kNeutralMode_Brake);
		m_motorFrontLeft->ConfigNeutralMode(CANSpeedController::kNeutralMode_Brake);
		m_motorRearRight->ConfigNeutralMode(CANSpeedController::kNeutralMode_Brake);
		m_motorFrontRight->ConfigNeutralMode(CANSpeedController::kNeutralMode_Brake);

		// arm
		//here we are creating and initializing a robot arm object
		pRobotArm.reset(new VictorSP(0));
		pArmStopper.reset(new DigitalInput(9));
		//m_armEncoder->SetDistancePerPulse(360.0 / (7.0 * 71.0));
		//m_armEncoder->SetMinRate(1.0);
		//m_armEncoder->SetMaxPeriod(0.5);

		//this initializes the timer
		m_autoTimer.Reset();

		//this is modulating the joystick input to motor using a parabola we made
		m_joystickCurve.SetPoints(0.5, 0.15, 1.0, 0.9);


		//this is initializing the digital input class to the corresponding object
		//this is using the input from the jumper cables
		// initialize our autonomous strategy
		m_autoSelect4.reset(new DigitalInput(4));
		m_autoSelect5.reset(new DigitalInput(5));
		m_autoSelect6.reset(new DigitalInput(6));
		//ChooseAutonomousStrategy();
		//m_pActiveAutoStrategy->RobotInit();

		//a function takes in information and spits it out. It is not owned by anything
		//a method is a function that is part of a class. It is owned by a class

		//chooser = new SendableChooser();
		//chooser->AddDefault(autoNameDefault, (void*)&autoNameDefault);
		//chooser->AddObject(autoNameCustom, (void*)&autoNameCustom);
		//SmartDashboard::PutData("Auto Modes", chooser);
	}


	/**
	 * This autonomous (along with the chooser code above) shows how to select between different autonomous modes
	 * using the dashboard. The sendable chooser code works with the Java SmartDashboard. If you prefer the LabVIEW
	 * Dashboard, remove all of the chooser code and uncomment the GetString line to get the auto name from the text box
	 * below the Gyro
	 *
	 * You can add additional auto modes by adding additional comparisons to the if-else structure below with additional strings.
	 * If using the SendableChooser make sure to add them to the chooser code above as well.
	 */

	//it is time to initialize everything for auto!!!
	void AutonomousInit()
	{
		//autoSelected = *((std::string*)chooser->GetSelected());
		//std::string autoSelected = SmartDashboard::GetString("Auto Selector", autoNameDefault);
		//std::cout << "Auto selected: " << autoSelected << std::endl;

		//this if else statement is part of autonomous initialization
		//we do not use this! It was part of the original robot class
		if(autoSelected == autoNameCustom){
			//Custom Auto goes here
		} else {
			//Default Auto goes here
		}

		//m_autoTimer.Reset();
		//m_autoTimer.Start();

		//m_motorRearLeft->Set(0.0);
		//m_motorFrontLeft->Set(0.0);
		//m_motorRearRight->Set(0.0);
		//m_motorFrontRight->Set(0.0);

		double motorSpeed = .5;

		m_autoTimer.Reset();
		m_autoTimer.Start();
		m_motorBall->Set(-.4);
		m_motorFrontLeft->Set(-motorSpeed);
		m_motorFrontRight->Set(motorSpeed);
		m_motorRearLeft->Set(-motorSpeed);
		m_motorRearRight->Set(motorSpeed);
	}

	//this makes us nervous
	void AutonomousPeriodic()
	{
		//if(autoSelected == autoNameCustom){
			//Custom Auto goes here
		//} else {
			//Default Auto goes here
		//}
		//m_pActiveAutoStrategy->AutonomousPeriodic();

		double autoTime = m_autoTimer.Get();
		if (Equals (autoTime, maxTime, 0.025) || autoTime > maxTime)
		{
			m_autoTimer.Reset();
			m_motorRearLeft->Set(0.0);
			m_motorFrontLeft->Set(0.0);
			m_motorRearRight->Set(0.0);
			m_motorFrontRight->Set(0.0);
			m_motorBall->Set(0.0);
			m_motorWinch->Set(0.0);
			m_motorScissor1->Set(0.0);
			m_motorScissor2->Set(0.0);
		}
	}


	//time to initialize the teleop mode!
	void TeleopInit()
	{
		//this line sets the initial joystick variables to avoid any errors or
		//problems with the joystick being sticky
		//takes all the starting variables as the 0
		m_controllerState.SetInitialJoystickVariables();
	}

	//periodic means it is recurring. It will continue over and over again
	//it is repeatedly called at a certain interval
	void TeleopPeriodic()
	{
		// Get state of the X-Box Controller
		m_controllerState.GetState();

		//What drive mode are we in
//		if (m_controllerState.GetRightJoystickButton())
//		{
//			RunCarMode();
//			//m_controllerState.SetRumbles(1,0.5);
//		}
//
//		else
//		{
//			RunTankMode();
//		}
		//These are the three methods that loop the whole time for teleop.
		RunTankMode();//this is for driving
		RunBallIntakeOuttake();//this is for the ball roller
		RotateRobotArm();//this is for the arm
	}

	//if you hit enable after putting the driver station you this will be called
	//we never use this so dont worry
	void TestPeriodic()
	{
		//lw->Run();
	}

	//this is called when you hit the disabled button on the driver station
	//it stops all of our robot motors. It does not matter what mode you are in
	void DisabledPeriodic()
	{
		m_motorRearLeft->Set(0.0);
		m_motorFrontLeft->Set(0.0);
		m_motorRearRight->Set(0.0);
		m_motorFrontRight->Set(0.0);
		m_motorBall->Set(0.0);
		m_motorWinch->Set(0.0);
		m_motorScissor1->Set(0.0);
		m_motorScissor2->Set(0.0);
	}


	//Tank Mode
	//This is a method is called a certain times per second. IT is the method that makes
	//us move.
	void RunTankMode()
	{
		// get raw joystick values and +- sign
		//this pulls the joystick values into the program.
		//(gets the state) gets what you want to do
		float leftX = m_controllerState.GetLeftY();
		float leftSign = (leftX >= 0.0) ? 1.0 : -1.0;
		float rightX = m_controllerState.GetRightY();
		float rightSign = (rightX >= 0.0) ? 1.0 : -1.0;

		//if the joystick values are almost the same, make them the same to make driving straight easier.
		if (Equals(fabs(leftX), fabs(rightX), .3))
		{
			if (fabs(leftX) < fabs(rightX))
			{
				rightX = leftX;
			}
			else
			{
				leftX = rightX;
			}
		}

		// compute the adjusted motor values based on joystick / motor curve
		//this takes the value and macthes it with the closest value on our parabola
		//so that the driving is smoother
		float leftY = leftSign * m_joystickCurve.FOfX(fabs(leftX));
		float rightY = rightSign * m_joystickCurve.FOfX(fabs(rightX));

		//This sets the motors the corresponding joystick values that we just did all
		//the math to!!!
		m_motorRearLeft->Set(leftY);
		m_motorFrontLeft->Set(leftY);
		m_motorRearRight->Set(rightY);
		m_motorFrontRight->Set(rightY);
	}


	//Car Mode - not used at the moment
	void RunCarMode()
	{
		m_motorRearLeft->Set(MotorSpeed(LEFT));
		m_motorFrontLeft->Set(MotorSpeed(LEFT));
		m_motorRearRight->Set(MotorSpeed(RIGHT));
		m_motorFrontRight->Set(MotorSpeed(RIGHT));
	}

	//Sets motor speed and turn
	float MotorSpeed (RobotSide side)
	{
		float leftY = fabs(m_controllerState.GetLeftY());
		float angle = atan2(m_controllerState.GetLeftX(),leftY)* 180.0
					/ 3.1415926;
		int index = static_cast<int>(((angle+90.0)/180.0)*16.0);
		if (side == LEFT)
		{
			index = 16-index;
		}

		//cout << "Left X = " << m_controllerState.GetLeftX() << "\n";
		//cout << "Left Y = " << leftY << "\n";

		float speed = m_controllerState.GetRightTrig() * m_motorSpeeds[index];
		if (side == LEFT)
		{
			speed *= -1.0;
		}

		if (m_controllerState.GetLeftJoystickButton())
		{
			speed *= -1.0;
		}

		return speed * 0.8; //adjust sensitivity
	}


	void RunBallIntakeOuttake()
	{
		if (m_controllerState.GetRightBumper())
		{
			// turn on ball outake
			m_motorBall->Set(1.0);
		}
		else if (m_controllerState.GetLeftBumper())
		{
			// turn on ball 'intake'
			m_motorBall->Set(-0.4);
		}
		else
		{ 	//turn off motor ball
			m_motorBall->Set(0.0);
		}
	}
	// we don't use this :/
	void AutoGoStraight()
	{
		double autoTime = m_autoTimer.Get();
		if (autoTime < 0.01)
		{
			m_autoTimer.Start(); //This is the starting point for Timer

			m_motorRearLeft->Set(-m_autoMotorSpeed);
			m_motorFrontLeft->Set(-m_autoMotorSpeed);
			m_motorRearRight->Set(m_autoMotorSpeed);
			m_motorFrontRight->Set(m_autoMotorSpeed);
		}
		else if (Equals (autoTime, m_autoGoStraightTime, 0.025) || autoTime > m_autoGoStraightTime)
		{
			m_motorRearLeft->Set(0.0);
			m_motorFrontLeft->Set(0.0);
			m_motorRearRight->Set(0.0);
			m_motorFrontRight->Set(0.0);
		}
	}

	void AutoProgramGoStraight(float distance)
	{
		float distances[2] = {48.0, 122.0};
		float times[2] = {1.18, 2.9};
		float motorTime = 0.0;
		float t = 0.0;

		if (distance < distances[0] || Equals(distance, distances[0], .01))
		{
			motorTime = (distance / distances[0]) * times[0];
		}


		else
		{
			t = (distance - distances[0]) / (distances[1] - distances[0]);
			motorTime = (times[0] * (1.0 - t)) + ((times[1] * t));
		}

		m_autoGoStraightTime = motorTime;
	}

	// we don't use this :/
	void AutoTurn(float autoTime, RobotSide side)
	{
		float autoMotorSpeed = (side == LEFT) ? .25 : -.25;
		m_motorRearLeft->Set(autoMotorSpeed);
		m_motorFrontLeft->Set(autoMotorSpeed);
		m_motorRearRight->Set(autoMotorSpeed);
		m_motorFrontRight->Set(autoMotorSpeed);
		Wait(autoTime);
		m_motorRearLeft->Set(0.0);
		m_motorFrontLeft->Set(0.0);
		m_motorRearRight->Set(0.0);
		m_motorFrontRight->Set(0.0);
	}

	void AutoProgramTurnRight()
	{
		AutoTurn(.65, RIGHT);
	}

	void AutoProgramTurnLeft()
	{
		AutoTurn(.65, LEFT);
	}
	void AutoProgramLowbarStraight ()
	{
		AutoProgramGoStraight(120.0);
	}

	//Checking if either trigger is held, if not, setting motor voltage to zero
	void RotateRobotArm ()
	{
		if(m_controllerState.GetRightTrig() > 0.0)
		{
			pRobotArm->Set(-m_downSensitivity * m_controllerState.GetRightTrig());
		}
		else if (m_controllerState.GetLeftTrig() > 0.0 && pArmStopper->Get())// if the switch is pressed, you cannot go down anymore
		{
			pRobotArm->Set(m_upSensitivity * m_controllerState.GetLeftTrig()); // If left bumper is held down & button is not pressed
		}
		else
		{
			pRobotArm->Set(0.0); // If nothing is held, set speed to 0%
		}
	}
};

START_ROBOT_CLASS(Robot)
