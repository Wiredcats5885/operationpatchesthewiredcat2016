#include "TouchRampStrategy.h"

TouchRampStrategy::TouchRampStrategy()
	: AutonomousStrategy(TOUCH_RAMP)
{
	m_breachSpeed = .01;
	m_approachDistance = 50.0;	// one move to ramp
}

TouchRampStrategy::~TouchRampStrategy()
{
}

void TouchRampStrategy::ProcessApproach()
{
	if (m_activeMode == APPROACH)
	{
		ProcessGoStraight(m_approachTime, m_approachSpeed, DISABLE);
	}
}

void TouchRampStrategy::ProcessBreach()
{
	// not used for just touching the ramp
}
