#pragma once
#include "AutonomousStrategy.h"

class RockWallStrategy : public AutonomousStrategy
{
public:
	RockWallStrategy();
	virtual ~RockWallStrategy();

// methods
protected:
	virtual void ProcessApproach();
	virtual void ProcessBreach();
};
