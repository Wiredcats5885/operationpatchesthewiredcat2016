#include "RockWallStrategy.h"

RockWallStrategy::RockWallStrategy()
	: AutonomousStrategy(ROCK_WALL)
{
	m_breachSpeed = 0.8;
	m_breachDistance = 130.0;
}

RockWallStrategy::~RockWallStrategy()
{
}

void RockWallStrategy::ProcessApproach()
{
	if (m_activeMode == APPROACH)
	{
		ProcessGoStraight(m_approachTime, m_approachSpeed, BREACH);
	}
}

void RockWallStrategy::ProcessBreach()
{
	if (m_activeMode == BREACH)
	{
		ProcessGoStraight(m_breachTime, m_breachSpeed, DISABLE);
	}
}
