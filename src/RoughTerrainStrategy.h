#pragma once
#include "AutonomousStrategy.h"

class RoughTerrainStrategy : public AutonomousStrategy
{
public:
	RoughTerrainStrategy();
	virtual ~RoughTerrainStrategy();

// methods
protected:
	virtual void ProcessApproach();
	virtual void ProcessBreach();
};
