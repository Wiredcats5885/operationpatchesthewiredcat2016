#pragma once

#include <AutonomousStrategy.h>

class RampartsStrategy: public AutonomousStrategy {
public:
	RampartsStrategy();
	virtual ~RampartsStrategy();

protected:
	virtual void ProcessApproach();
	virtual void ProcessBreach();
};

