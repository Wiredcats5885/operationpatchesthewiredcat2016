#include "Definitions.h"
#include "AutonomousStrategy.h"

AutonomousStrategy::AutonomousStrategy(AutonomousDefenses defenseType)
	: m_defenseType(defenseType)
	, m_activeMode(WAIT)
	, m_waitTime(0.0)
	, m_approachTime(0.0)
	, m_breachTime(0.0)
	, m_approachSpeed(.5)
	, m_breachSpeed(.8)
	, m_approachDistance(74.0)	// ramp @ 122" breach from 48" away
	, m_breachDistance(144.0)	// 48" to ramp + 48" to go over ramp + go 48" past to clear back of robot
{
	m_timer.Reset();
}

AutonomousStrategy::~AutonomousStrategy()
{
}

void AutonomousStrategy::RobotInit()
{
	m_motorRearLeft.reset(new CANTalon(MOTOR_REAR_LEFT));
	m_motorFrontLeft.reset(new CANTalon(MOTOR_FRONT_LEFT));
	m_motorFrontRight.reset(new CANTalon(MOTOR_FRONT_RIGHT));
	m_motorRearRight.reset(new CANTalon(MOTOR_REAR_RIGHT));
	m_motorBall.reset(new CANTalon(MOTOR_BALL));

	m_motorRearLeft->SetControlMode(CANSpeedController::kPercentVbus);
	m_motorFrontLeft->SetControlMode(CANSpeedController::kPercentVbus);
	m_motorFrontRight->SetControlMode(CANSpeedController::kPercentVbus);
	m_motorRearRight->SetControlMode(CANSpeedController::kPercentVbus);
	m_motorBall->SetControlMode(CANSpeedController::kPercentVbus);

	m_motorRearLeft->ConfigNeutralMode(CANSpeedController::kNeutralMode_Brake);
	m_motorFrontLeft->ConfigNeutralMode(CANSpeedController::kNeutralMode_Brake);
	m_motorRearRight->ConfigNeutralMode(CANSpeedController::kNeutralMode_Brake);
	m_motorFrontRight->ConfigNeutralMode(CANSpeedController::kNeutralMode_Brake);

	m_approachTime = CalculateMotorOnTime(m_approachDistance, 1.0);
	m_breachTime = CalculateMotorOnTime(m_breachDistance, (m_approachSpeed/m_breachSpeed));
}

void AutonomousStrategy::ProcessGoStraight(float maxTime, float motorSpeed, AutonomousMode newMode)
{
	double autoTime = m_timer.Get();

	if (autoTime < 0.01)
	{
		m_timer.Start(); //This is the starting point for Timer

		m_motorRearLeft->Set(-motorSpeed);
		m_motorFrontLeft->Set(-motorSpeed);
		m_motorRearRight->Set(motorSpeed);
		m_motorFrontRight->Set(motorSpeed);
	}
	else if (Equals (autoTime, maxTime, 0.025) || autoTime > maxTime)
	{
		m_timer.Reset();
		m_activeMode = newMode;	// set to next mode
	}
	else
	{
		m_motorRearLeft->Set(-motorSpeed);
		m_motorFrontLeft->Set(-motorSpeed);
		m_motorRearRight->Set(motorSpeed);
		m_motorFrontRight->Set(motorSpeed);
	}
}

float AutonomousStrategy::CalculateMotorOnTime(float distance, float scaleFactor)
{
	float distances[2] = {48.0, 122.0};
	float times[2] = {1.18, 3.3};
	float motorTime = 0.0;
	float t = 0.0;

	if (distance < distances[0] || Equals(distance, distances[0], .01))
	{
		motorTime = (distance / distances[0]) * times[0];
	}
	else
	{
		t = (distance - distances[0]) / (distances[1] - distances[0]);
		motorTime = (times[0] * (1.0 - t)) + ((times[1] * t));
	}

	return (motorTime * scaleFactor);
}

void AutonomousStrategy::ProcessWaiting()
{
	if (m_activeMode != WAIT)
	{
		return;
	}

	double autoTime = m_timer.Get();

	if (Equals(m_waitTime, 0.0, 0.001) || autoTime > m_waitTime)
	{
		m_motorBall->Set(-0.4); //Hold onto ball while going over defense
		m_timer.Reset();
		m_activeMode = APPROACH;	// set to next mode
		return;
	}

	if (autoTime < 0.001)
	{
		m_timer.Start();
	}
}

void AutonomousStrategy::ProcessDisable()
{
	if (m_activeMode == DISABLE)
	{
		m_motorRearLeft->Set(0.0);
		m_motorFrontLeft->Set(0.0);
		m_motorRearRight->Set(0.0);
		m_motorFrontRight->Set(0.0);
		m_motorBall->Set(0.0);
	}
}

void AutonomousStrategy::AutonomousPeriodic()
{
	ProcessWaiting();
	ProcessApproach();
	ProcessBreach();
	ProcessDisable();
}
